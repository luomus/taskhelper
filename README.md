## Description

[TaskHelper](https://bitbucket.org/luomus/taskhelper) is an api used for two things:

1. Link Trello cards with Pivotal epics
2. task.dev.laji.fi login redirect helper

Written with [Nest](https://github.com/nestjs/nest).

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Documentation
```bash
# build the docs
$ yarn run build:docs

# open docs
$ open docs/index.html

```


## License

TaskHelper is [MIT licensed](LICENSE).
