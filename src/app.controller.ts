import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { AppService, IChangedDataResponse } from './services/app.service';
import { Observable, of } from 'rxjs';
import { ITrelloWebhook, TrelloService } from './services/trello.service';
import { IPivotalWebHook, PivotalService } from './services/pivotal.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/task-login-redirect')
  loginRedirect(@Req() request, @Res() response): any {
    return this.appService.redirectGetToLogin(request, response);
  }

  @Post('/task-login-redirect')
  loginPostRedirect(@Req() request, @Res() response): any {
    return this.appService.redirectPostToLogin(request, response);
  }

  @Get('/link-trello-and-pivotal')
  link(): Observable<IChangedDataResponse> {
    return this.appService.linkPivotalAndTrello();
  }

  @Get('/event')
  getEvents(): Observable<string> {
    return of('listening...');
  }

  @Post('/event')
  postEvents(@Body() event: ITrelloWebhook|IPivotalWebHook): Observable<string> {
    if (TrelloService.isTrelloWebhook(event)) {
      if (event.action.type) {
        this.appService.linkPivotalAndTrello().subscribe();
      } else {
        console.log('Trello activity that didn\'t trigger an update', event);
      }
    } else if (PivotalService.isPivotalWebhook(event)) {
      if (event.kind === 'story_update_activity') {
        this.appService.linkPivotalAndTrello().subscribe();
      } else {
        console.log('Pivotal activity that didn\'t trigger an update', event);
      }
    }
    return of('received');
  }

}
