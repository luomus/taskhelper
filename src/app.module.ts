import { HttpModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './services/app.service';
import { TrelloService } from './services/trello.service';
import { PivotalService } from './services/pivotal.service';
import { ConfigService } from './services/config.service';
import { UtilService } from './services/util.service';

@Module({
  imports: [HttpModule],
  controllers: [AppController],
  providers: [
    AppService,
    TrelloService,
    PivotalService,
    {
      provide: ConfigService,
      useValue: new ConfigService(`${process.env.NODE_ENV}.env`)
    },
    UtilService
  ]
})
export class AppModule {}
