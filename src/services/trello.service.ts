import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from './config.service';
import { Observable } from 'rxjs';
import { UtilService } from './util.service';
import { map } from 'rxjs/operators';

export interface ICard {
  id: string;
  name: string;
  desc: string;
  closed: boolean;
  due: any;
  shortUrl: string;
}

export interface ITrelloWebhook {
  action: {
    type: string
  };
}

@Injectable()
export class TrelloService {

  constructor(
    private http: HttpService,
    private config: ConfigService
  ) {}

  static isTrelloWebhook(event: any): event is ITrelloWebhook {
    return event && typeof (event as ITrelloWebhook).action !== 'undefined';
  }

  /**
   * Fetch all open cards
   */
  getCards(): Observable<ICard[]> {
    return this.http.get(this.getUrl('boards/' + this.config.get('TRELLO_BOARD') + '/lists', {
      fields: 'name',
      filter: 'open',
      cards: 'open',
      card_fields: 'id,name,desc,due,shortUrl'
    })).pipe(
      map(res => res.data || []),
      map((lists: any[]) => lists.reduce((cumulative: ICard[], current) => {
        if ((current.name || '').trim().startsWith('*')) {
          return cumulative;
        }
        if (current.cards) {
          cumulative.push(...current.cards);
        }
        return cumulative;
      }, [])),
      map(cards => cards.filter(card => !card.name.trim().startsWith('*')))
    );
  }

  /**
   * Updated cards title
   * @param id cards id
   * @param title cards title
   */
  updateTitle(id: string, title: string): Observable<ICard> {
    return this.http.put(this.getUrl('cards/' + id + '/name', {value: title})).pipe(
      map(res => res.data)
    );
  }

  private getUrl(resource: string, query?: {[key: string]: string}): string {
    return this.config.get('TRELLO_API') + resource + UtilService.toQueryString({
      ...query,
      key: this.config.get('TRELLO_API_KEY'),
      token: this.config.get('TRELLO_TOKEN')
    });
  }

}
