import { HttpService, Injectable } from '@nestjs/common';
import { ICard, TrelloService } from './trello.service';
import { IEpic, IPivotalUpdater, PivotalService } from './pivotal.service';
import { from, merge, Observable, of } from 'rxjs';
import { catchError, concatMap, delay, last, map, mergeMap, tap } from 'rxjs/operators';
import { ConfigService } from './config.service';
import { Request, Response } from 'express';

interface ICardTitleUpdate {
  id: string;
  title: string;
}

interface IChangedData {
  newEpics: IEpic[];
  updateEpics: IEpic[];
  updateCards: ICardTitleUpdate[];
}

export interface IChangedDataResponse extends IChangedData {
  status: string;
}

const PIVOTAL_STATUS_IN_CARD_SPLITTER = ' | ';

@Injectable()
export class AppService {

  constructor(
    private trelloService: TrelloService,
    private pivotalService: PivotalService,
    private config: ConfigService,
    private http: HttpService
  ) {}

  async redirectGetToLogin(request: Request, response: Response): Promise<any> {
    const token = request.query.token as string || '';
		this.redirectToLogin(response, token, 303);
  }

  async redirectPostToLogin(request: Request, response: Response): Promise<any> {
    const token = request.body.token as string || '';
		this.redirectToLogin(response, token, 307);
  }

	private async redirectToLogin(response: Response, token: string, status: number): Promise<any> {
    let sub = '';
		let taskId = undefined;
    let isVir = false;
    try {
      const tokenUrl = this.config.get('TOKEN_CHECK_URL').replace('%token%', token);
      const tokenData = await this.http.get(tokenUrl).pipe(map(res => res.data)).toPromise();
      sub = tokenData?.next || '';
      taskId = sub.match(/^([0-9]+)$/)?.[1];
      if (tokenData?.target === this.config.get('VIR_SYSTEM_ID')) {
        isVir = true;
      }
    } catch (e) {
      console.log(e);
    }

    const target = this.config.get(isVir ? 'VIR_LOGIN_REDIRECT_URL' : 'LOGIN_REDIRECT_URL')
      .replace('%token%', token)
      .replace('%sub%', taskId ?  `${taskId}.` : '');

    return response.redirect(status, target);
	}

  /**
   * Link Pivotal epics with Trello cards
   */
  linkPivotalAndTrello(): Observable<IChangedDataResponse> {
    let errors: boolean|string = false;

    const pickError = (e) => {
      if (e.response && e.response.data && e.response.data.general_problem) {
        errors = e.response.data.general_problem;
      } else if (errors === false) {
        errors = true;
      }
    };

    const updatePivotal$ = (data: IEpic[], method: keyof IPivotalUpdater): Observable<IEpic> => {
      return data.length === 0 ? of(null) : from(data).pipe(
        delay(3000),
        concatMap((epic: IEpic) => this.pivotalService[method](epic).pipe(catchError((e) => {
          pickError(e);
          return of(null);
        })))
      );
    };

    const updateCards$ = (data: ICardTitleUpdate[]): Observable<ICard> => {
      return data.length === 0 ? of(null) : from(data).pipe(
        concatMap(update => this.trelloService.updateTitle(update.id, update.title)),
      );
    };

    return this.trelloService.getCards().pipe(
      mergeMap((cards) => this.pivotalService.getEpics().pipe(
        map(epics => {
          return this.getChanges(cards, epics);
        }),
        mergeMap(updates => merge(
          updateCards$(updates.updateCards),
          updatePivotal$(updates.updateEpics, 'updateEpic'),
          updatePivotal$(updates.newEpics, 'addEpic'),
        ).pipe(
          last(),
          map(() => ({...updates, status: errors ? (typeof errors !== 'boolean' ? errors : 'failed') : 'ok'}))
        ))
      ))
    );
  }

  private getChanges(cards: ICard[], epics: IEpic[]): IChangedData {
    return cards.reduce((cumulative, current) => {
      const epicFromCard = this.cardToEpic(current);
      if (epicFromCard.name.length >= 40) {
        return cumulative;
      }
      const linkedEpicIdx = epics.findIndex(epic => AppService.isEpicLinkedToCard(epic, current.shortUrl));
      if (linkedEpicIdx === -1) {
        cumulative.newEpics.push(epicFromCard);
      } else {
        const doneStr = AppService.epicCompleteness(epics[linkedEpicIdx]);
        if (!AppService.isSameEpic(epics[linkedEpicIdx], epicFromCard)) {
          cumulative.updateEpics.push(this.cardToEpic(current, epics[linkedEpicIdx]));
        }
        if (!current.name.endsWith(doneStr)) {
          cumulative.updateCards.push({
            id: current.id,
            title: AppService.removeAllAfterLast(current.name, PIVOTAL_STATUS_IN_CARD_SPLITTER) + PIVOTAL_STATUS_IN_CARD_SPLITTER + doneStr
          });
        }
      }
      return cumulative;
    }, {newEpics: [], updateEpics: [], updateCards: []} as IChangedData);
  }

  private cardToEpic(card: ICard, base?: IEpic): IEpic {
    return {
      ...base,
      project_id: Number(this.config.get('PIVOTAL_PROJECT_ID')),
      name: AppService.removeAllAfterLast(AppService.removeAllAfterLast(card.name, PIVOTAL_STATUS_IN_CARD_SPLITTER), '(').trim(),
      description: `[Trello](${card.shortUrl})
      ${card.desc}`.trim()
    };
  }

  private static isEpicLinkedToCard(epic: IEpic, url: string): boolean {
    return epic.description && epic.description.includes(`(${url})`);
  }

  private static epicCompleteness(epic: IEpic): string {
    const counts = epic.label.counts.sum_of_story_estimates_by_state;
    const done = counts.accepted + counts.finished + counts.delivered;
    const total = done + counts.planned + counts.rejected + counts.started + counts.unscheduled + counts.unstarted;
    return total > 0 ? `${done}/${total} (${Math.round((done / total) * 100)}%)` : 'no tasks';
  }

  private static isSameEpic(current: IEpic, fromCard: IEpic): boolean {
    return current.name === fromCard.name && current.description === fromCard.description;
  }

  private static removeAllAfterLast(subject: string, mark: string): string {
    const parts = subject.split(mark);
    if (parts.length > 1) {
      parts.pop();
    }
    return parts.join(mark);
  }
}
