import { Injectable } from '@nestjs/common';
import { parse as dotenvParse } from 'dotenv';
import { readFileSync as fsReadFileSync } from 'fs';

interface IEnvConf {
  TRELLO_API: string;
  TRELLO_API_KEY: string;
  TRELLO_TOKEN: string;
  TRELLO_BOARD: string;
  PIVOTAL_API: string;
  PIVOTAL_TOKEN: string;
  PIVOTAL_PROJECT_ID: string;
  TOKEN_CHECK_URL: string;
  LOGIN_REDIRECT_URL: string;
  VIR_LOGIN_REDIRECT_URL: string;
  VIR_SYSTEM_ID: string;
}

/**
 * Configuration service to pick needed values from os environment
 *
 * See [[IEnvConf]] interface for all the possible configuration parameters
 */
@Injectable()
export class ConfigService {

  private readonly envConfig: IEnvConf;

  constructor(filePath: string) {
    this.envConfig = dotenvParse(fsReadFileSync(filePath));
  }

  /**
   * Fetch configuration value
   *
   * @param key
   */
  get(key: keyof IEnvConf): string {
    return this.envConfig[key];
  }

}
