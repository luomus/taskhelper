import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from './config.service';
import { UtilService } from './util.service';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

export interface IEpic {
  id?: number;
  project_id: number;
  name: string;
  description?: string;
  label?: {
    id?: number;
    name: string;
    counts: {
      sum_of_story_estimates_by_state: {
        accepted: number;
        started: number;
        finished: number;
        unstarted: number;
        planned: number;
        delivered: number;
        unscheduled: number;
        rejected: number;
      }
    }
  };
}

export interface IPivotalUpdater {
  addEpic(epic: IEpic): Observable<IEpic>;
  updateEpic(epic: IEpic): Observable<IEpic>;
}

export interface IPivotalWebHook {
  kind: 'story_update_activity';
}

/**
 * Service to communicate with pivotal tracker
 */
@Injectable()
export class PivotalService implements IPivotalUpdater {

  constructor(
    private http: HttpService,
    private config: ConfigService
  ) {}

  static isPivotalWebhook(event: any): event is IPivotalWebHook {
    return event && typeof (event as IPivotalWebHook).kind !== 'undefined';
  }

  /**
   * Fetch all epics
   */
  getEpics(): Observable<IEpic[]> {
    return this.http.get(this.getUrl('epics', {
      fields: 'project_id,name,description,label(id,name,counts)',
    }), this.httpConfig()).pipe(
      map(res => res.data || [])
    );
  }

  /**
   * Add new epic
   *
   * @param epic data to be saved
   */
  addEpic(epic: IEpic): Observable<IEpic> {
    return this.http.post(this.getUrl('epics'), epic, this.httpConfig()).pipe(
      map(res => res.data)
    );
  }

  /**
   * Update existing epic data and try to update label data to match the new name
   * (updating label doesn't work atm because api is returning 403 for some reason)
   *
   * @param epicData data to pe updated
   */
  updateEpic(epicData: IEpic): Observable<IEpic> {
    return this.http.put(this.getUrl('epics/' + epicData.id), epicData, this.httpConfig()).pipe(
      map(res => res.data),
      switchMap(epic => PivotalService.isEpicAndLabelSame(epic) ?
        of(epic) :
        this.http.put(this.getUrl('labels/' + epic.label.id, {
          name: epic.name.toLocaleLowerCase()
        }), this.httpConfig()).pipe(
          map(() => epic),
          catchError(() => of(epic))
        )
      )
    );
  }

  private static isEpicAndLabelSame(epic: IEpic): boolean {
    return !(epic.label && epic.label.name !== epic.name);
  }

  private httpConfig() {
    return {headers: {'X-TrackerToken': this.config.get('PIVOTAL_TOKEN')}};
  }

  private getUrl(resource: string, query?: {[key: string]: string}): string {
    return this.config.get('PIVOTAL_API') + 'projects/' + this.config.get('PIVOTAL_PROJECT_ID') + '/' + resource + UtilService.toQueryString(query);
  }

}
