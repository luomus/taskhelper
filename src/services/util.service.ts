import { Injectable } from '@nestjs/common';

@Injectable()
export class UtilService {

  /**
   * Convert query object to query string
   *
   * @param obj key value object where both are strings
   */
  public static toQueryString(obj: {[key: string]: string}): string {
    if (!obj) {
      return '';
    }
    const keys = Object.keys(obj);
    if (!keys.length) {
      return '';
    }
    return '?' + keys.reduce((a, k) => {
        a.push(k + '=' + encodeURIComponent(obj[k]));
        return a;
      }, []).join('&');
  }

}
